<?php 
include 'ActiveRecord.php';
  ActiveRecord\Config::initialize(function($con){
  	$con->set_model_directory('models');
  	$con->set_connections(['development'=> 'mysql://root:passwd@localhost/gradebook']);
  });

  if(isset($_POST['create'])){
  	$name = $_POST['name'];
  	$code = $_POST['code'];

  	$subject = new Subject;
  	$subject->name = $name;
  	$subject->code = $code;
  	$subject->save();
  }

  if(isset($_POST['update'])){
  	$id = $_POST['id'];
  	$name = $_POST['name'];
  	$code = $_POST['code'];

  	$subject = Subject::find($id);
  	$subject->name = $name;
  	$subject->code = $code;
  	$subject->save();
  }

?>
<h3>Create New Subject:</h3>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
	<input type="text" name='name' placeholder="My Subject" required>
	<input type="text" name='code' placeholder="AL547" required size=8>
	<button type="submit" name="create">Create</button>
</form>
<h3>Update Subject Attributes</h3>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
	<select name="id" required>
		<?php foreach(Subject::all() as $subject): ?>
			<option value="<?php echo $subject->id ?>"><?php echo $subject->id ?></option>
		<?php endforeach;?>
		

	</select>
	<input type="text" name='name' placeholder="My Subject" required>
	<input type="text" name='code' placeholder="AL547" required size=8>
	<button type="submit" name="update">Update</button>
</form>
<h3>Subjects:</h3>
<table border=1>
	<tr>
		<th>ID</th>
		<th>Subject</th>
		<th>Code</th>
	</tr>

<?php 
 foreach (Subject::all() as $subject){
 	echo '<tr>';
 	echo '<td>'.$subject->id."</td>";
 	echo '<td>'.$subject->name."</td>";
 	echo '<td>'.$subject->code."</td>";
 	echo '</tr>';
 }
?>
</table>