<?php 
  include 'ActiveRecord.php';
  ActiveRecord\Config::initialize(function($con){
  	$con->set_model_directory('models');
  	$con->set_connections(['development'=> 'mysql://root:passwd@localhost/gradebook']);
  });
  

  if(isset($_POST['add_student'])){
  	$fname = $_POST['firstname'];
  	$lname = $_POST['lastname'];
  	$student = new Student;
  	$student->firstname = $fname;
  	$student->lastname = $lname;
  	$student->save();
  	
  }

  if(isset($_POST['assign_subject'])){
  	$studentId = $_POST['student_id'];
  	$subjectId = $_POST['subject_id'];
  	$subject = Subject::find($subjectId);
  	$student = Student::find($studentId);
	$student->create_subjects(['name'=>$subject->name,'code'=>$subject->code]);

  }



?>
<h3>Add Student:</h3>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
	<input type="text" name='firstname' placeholder="John"> 
	<input type="text" name='lastname' placeholder="Doe"> 
	<input type="text" name='year' placeholder="3"> 
	<button type="submit" name="add_student">Add Student</button>
</form>

<h3>Assign Subject:</h3>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
	<select name="subject_id">
		<?php foreach(Subject::all(['group'=>'name']) as $subject):?>
			<option value="<?php echo $subject->id ?>"><?php echo $subject->name?></option>
		<?php endforeach;?>
	</select>
	<select name="student_id">
		<?php foreach(Student::all() as $student):?>
			<option value="<?php echo $student->id ?>"><?php echo $student->firstname?> <?php echo $student->lastname?></option>
		<?php endforeach;?>
	</select>
	<button type="submit" name="assign_subject">Assign Subject</button>
</form>
<?php 
  $students = Student::all();

 foreach ($students as $student){
 	echo $student->firstname." ".$student->lastname."<br>";
 	echo '<ul>';
 		foreach($student->subjects as $subject)
 		echo '<li>'.$subject->name." ".$subject->code.'</li>';
 	echo '</ul>';
 }
?>